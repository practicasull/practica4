#include "Estado.h"

Estado::Estado(){}

Estado::Estado(const int& i, std::fstream& file)
{

    id_ = i;

    file >> aceptacion >> n_transiciones;

    for (int j=0;j<n_transiciones;++j) {

        transiciones_.insert(Transicion(j,file));

    }

}


Estado::Estado(const int& i):
        id_(i)
{}

Estado::Estado(const Estado& E)
{
    this->id_ = E.getId_();
    this->n_transiciones = E.getN_transiciones();
    this->aceptacion = E.getAceptacion();
    this->transiciones_ = E.transiciones_;
}



Estado::~Estado(){}

unsigned int Estado::getAceptacion() const
{
    return aceptacion;
}

unsigned int Estado::getN_transiciones() const
{
    return n_transiciones;
}

unsigned int Estado::getId_() const
{
    return id_;
}
/*
void Estado::comprobar_cadena(char i, int &x, int &y, bool &fallo)const
{

    for(int j=0;j<n_transiciones;j++){

        int aux;

        aux = obtener_siguiente(i);

        if(aux != -1){		// Si hay un caracter que no pertenece a la cadena, no continua

            y = aux;

            std::cout << x << "                " << i << "          " << y << std::endl;

            x = obtener_siguiente(i);

            break;

        }else{

            fallo = true;
        }
    }
}
*/
std::set<unsigned int> Estado::obtener_siguiente(const char& i) const
{
    std::set<unsigned int> s_aux;

    for (auto &k: transiciones_) {

        if (k.getCaracter() == i || k.getCaracter() == '~')

            s_aux.insert(k.getEstado_sig());

    }

    return s_aux;
}


bool Estado::operator<(const Estado& rhs) const
{
    return id_ < rhs.id_;
}

bool Estado::operator>(const Estado& rhs) const
{
    return rhs < *this;
}

bool Estado::operator<=(const Estado& rhs) const
{
    return !(rhs < *this);
}

bool Estado::operator>=(const Estado& rhs) const
{
    return !(*this < rhs);
}

std::ostream& operator<<(std::ostream& os, const Estado& Estado)
{

    os << Estado.id_<< " " << Estado.aceptacion << " " << Estado.n_transiciones;
    for (auto &i: Estado.transiciones_)
        os << i;

    return os;
}
