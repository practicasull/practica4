#pragma once

#include <iostream>
#include <fstream>
#include <set>
#include <string>

#include "Estado.h"


class NFA
{

private:

    std::set<Estado> estados_;
    unsigned int estado_inicial_;
    unsigned int n_estados_;

public:

    NFA(std::string nombrefichero, int &error);
    ~NFA();

    void comprobar_cadena(const std::string& cadena);

    void obtener_caminos(std::vector< std::pair<unsigned int, unsigned int> > solucion, const Estado& estado, const std::string& cadena, unsigned int pos);

    friend std::ostream& operator<<(std::ostream& os, const NFA& nfa);

};
