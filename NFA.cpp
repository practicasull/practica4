#include "NFA.h"

NFA::NFA(std::string nombrefichero, int &error){

    std::fstream file;
    file.open(nombrefichero);

    if(!file.is_open()){

        std::cerr << "Error en la apertura del fichero" << std::endl;
        error = 1;

    }

    else{

        error = 0;

        file >> n_estados_;
        file >> estado_inicial_;

        estados_.clear();

        for(int i=0;i<n_estados_;i++){

            int x;

            file >> x;

            estados_.insert(Estado(x,file));
        }
    }
}

NFA::~NFA()
{}

void NFA::comprobar_cadena(const std::string& cadena)
{
    unsigned int estado_actual = estado_inicial_;

    unsigned int pos = 0;

    std::vector< std::pair<unsigned int, unsigned int> > solucion;

    do{

        std::set<unsigned int> caminos;

        // Como es set tenemos que encontrar el iterador en el que se encuentra el estado actual. Y una vez obtenido con
        // el find() trabajamos con el con (*...).obtener_siguiente();
        caminos = (*estados_.find(Estado(estado_actual))).obtener_siguiente(cadena[pos]);

        if (caminos.size() > 1) {

            pos++;

            if (pos == cadena.length()) {

                std::cout << "Estado Actual" << "  " << "Estado Siguiente" << std::endl;

                for (auto &i: solucion) {

                    std::cout << i.first << "  " << i.second << std::endl;

                }

            }

            for (auto &i: caminos) {

                obtener_caminos(solucion, (*estados_.find(Estado(i))), cadena, pos);

            }

        }

        else{

            std::pair<unsigned int, unsigned int> aux;

            aux.first = estado_actual;
            aux.second = (*caminos.begin());


            solucion.push_back(aux);

            pos++;
            if (pos == cadena.length()) {

                std::cout << "Estado Actual" << "  " << "Estado Siguiente" << std::endl;

                for (auto &i: solucion) {

                    std::cout << i.first << "  " << i.second << std::endl;

                }

            }
            estado_actual = aux.second;
        }


    }while (cadena.length() > pos);


}

void NFA::obtener_caminos(std::vector<std::pair<unsigned int, unsigned int> > solucion, const Estado& estado,
                          const std::string& cadena, unsigned int pos)
{

    std::vector< std::pair<unsigned int, unsigned int> > solucion_aux;

    unsigned int estado_actual = estado.getId_();

    std::set<unsigned int> caminos;
    caminos = estado.obtener_siguiente(cadena[pos]);


    if (caminos.size() > 1) {


        for (auto &i: caminos) {

            obtener_caminos(solucion_aux, (*estados_.find(Estado(i))), cadena, pos);

        }

        pos++;
        if (pos == cadena.length()) {


            std::pair<unsigned int, unsigned int> aux;

            aux.first = estado.getId_();

            for (auto &i: caminos) {


                aux.second = (*estados_.find(Estado(i))).getId_();
                solucion.push_back(aux);

                std::cout << "Estado Actual" << "  " << "Estado Siguiente" << std::endl;

                for(auto &j: solucion) {

                    std::cout << j.first << "  " << j.second << std::endl;

                }

                solucion.pop_back();



            }
        }
    }

    else{

        std::pair<unsigned int, unsigned int> aux;

        aux.first = estado_actual;
        aux.second = (*caminos.begin());


        solucion.push_back(aux);

        pos++;
        if (pos == cadena.length()) {

            std::cout << "Estado Actual" << "  " << "Estado Siguiente" << std::endl;

            for (auto &i: solucion) {

                std::cout << i.first << "  " << i.second << std::endl;

            }

        }
        estado_actual = aux.second;
    }

    /*

    std::pair<unsigned int, unsigned int> aux;

    aux.first = estado.getId_();
    aux.second = (*estados_.find(Estado(*s_aux.begin()))).getId_();


    solucion.push_back(aux);

     */

}

std::ostream& operator<<(std::ostream& os, const NFA& nfa)
{
    os << nfa.n_estados_ << std::endl;
    os << nfa.estado_inicial_ << std::endl;

    for (auto &i: nfa.estados_)

        os << i << std::endl;

    return os;
}


