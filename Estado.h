#pragma once

#include <iostream>
#include <fstream>
#include <set>
#include <string>

#include "transiciones.h"

class Estado
{

private:

    unsigned int aceptacion;		// es un estado de aceptacion?
    unsigned int n_transiciones;	// numero de transiciones
    unsigned int id_;

public:

    std::set<Transicion> transiciones_;

public:

    Estado(const int& i, std::fstream& fstream);
    Estado(const int& i);
    Estado(const Estado& E);
    Estado();
    ~Estado();

    unsigned int getAceptacion() const;

    unsigned int getN_transiciones() const;

    unsigned int getId_() const;

    // COMPROBAR SI UNA CADENA ES ACEPTADA O NO

    //void comprobar_cadena(char i, int &x, int &y, bool &fallo) const;

    std::set<unsigned int> obtener_siguiente(const char& i) const;

    bool operator<(const Estado& rhs) const;

    bool operator>(const Estado& rhs) const;

    bool operator<=(const Estado& rhs) const;

    bool operator>=(const Estado& rhs) const;

    friend std::ostream& operator<<(std::ostream& os, const Estado& Estado);

};
