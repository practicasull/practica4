#include "transiciones.h"

Transicion::Transicion(){}

Transicion::Transicion(const Transicion& t)
{
	this->id_ = t.id_;
	this->caracter = t.caracter;
	this->estado_sig = t.estado_sig;
}

Transicion::Transicion(const int& i, std::fstream& file)
{

	char c;
	int x;

	file >> c >> x;

	id_ = i;
	caracter = c;
	estado_sig = x;

}

Transicion::~Transicion(){}

bool Transicion::operator<(const Transicion& rhs) const
{
	return id_ < rhs.id_;
}

bool Transicion::operator>(const Transicion& rhs) const
{
	return rhs < *this;
}

bool Transicion::operator<=(const Transicion& rhs) const
{
	return !(rhs < *this);
}

bool Transicion::operator>=(const Transicion& rhs) const
{
	return !(*this < rhs);
}

std::ostream& operator<<(std::ostream& os, const Transicion& transicion)
{

	os  << " " << transicion.caracter << " " << transicion.estado_sig;
	return os;
}

unsigned int Transicion::getEstado_sig() const
{
	return estado_sig;
}

char Transicion::getCaracter() const
{
	return caracter;
}
