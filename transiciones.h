#pragma once

#include <fstream>
#include <vector>
#include <iostream>

class Transicion{

private:

	int id_;				// Identificador para ordenarlo en el set de la clase estado
	char caracter;
	unsigned int estado_sig;

public:

	Transicion();
	Transicion(const Transicion& t);
	Transicion(const int& i, std::fstream& file);
	~Transicion();

	unsigned int getEstado_sig() const;

	char getCaracter() const;

	bool operator<(const Transicion& rhs) const;

	bool operator>(const Transicion& rhs) const;

	bool operator<=(const Transicion& rhs) const;

	bool operator>=(const Transicion& rhs) const;

	friend std::ostream& operator<<(std::ostream& os, const Transicion& transicion);

};